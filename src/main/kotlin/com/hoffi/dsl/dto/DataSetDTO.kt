package com.hoffi.dsl.dto

class DataSetDTO : IDTO {
    companion object { val NULL = DataSetDTO() }

    var field1: String = "field1Value"
    var field2: String = "field2Value"
    var field3: String = "field3Value"
    var field4: String = "field4Value"
    var fieldInt: Int = 3
    var dataSetInnerDTO: DataSetInnerDTO = DataSetInnerDTO()
}

class DataSetInnerDTO : IDTO {
    companion object { val NULL = DataSetInnerDTO() }
    var inner: String = "innerField"
}
