package com.hoffi.dsl.expr

import com.hoffi.dsl.ex.DslException
import com.hoffi.dsl.ex.RuleDslEvalException
import kotlinx.datetime.Instant
import mu.KotlinLogging

interface IBoolExpr {
    fun eval(ctx: Context): Boolean
}
sealed class BoolExpr: IBoolExpr {
    object NULL : BoolExpr() { override fun eval(ctx: Context): Boolean = throw DslException("eval called on BoolExpr.NULL") }
    class not(val nestingLevel: Int, val description: String, val notExprs: MutableList<BoolExpr> = mutableListOf()) : BoolExpr() {
        private val log = KotlinLogging.logger(this::class.qualifiedName!!)
        override fun toString(): String = "${this::class.simpleName}[description:${description}, #exprs=${notExprs.size}"
        fun add(vararg expr: BoolExpr): BoolExpr { notExprs.addAll(expr) ; return this }
        fun add(exprs: List<BoolExpr>): BoolExpr { notExprs.addAll(exprs) ; return this }
        override fun eval(ctx: Context): Boolean {
            log.debug { String.format("eL%d->       %s", nestingLevel, this) }
            val boolOutcome = !notExprs.first().eval(ctx)
            log.debug { String.format("eL%d<- %-5s %s", nestingLevel, boolOutcome, this) }
            return boolOutcome
        }
    }
    class or(val nestingLevel: Int, val description: String, val orExprs: MutableList<BoolExpr> = mutableListOf()) : BoolExpr() {
        private val log = KotlinLogging.logger(this::class.qualifiedName!!)
        override fun toString(): String = "${this::class.simpleName}[description:${description}, #exprs=${orExprs.size}"
        fun add(vararg expr: BoolExpr): BoolExpr { orExprs.addAll(expr) ; return this }
        fun add(exprs: List<BoolExpr>): BoolExpr { orExprs.addAll(exprs) ; return this }
        override fun eval(ctx: Context): Boolean {
            log.debug { String.format("eL%d->       %s", nestingLevel, this) }
            val boolOutcome = orExprs.firstOrNull { it.eval(ctx) } != null
            log.debug { String.format("eL%d<- %-5s %s", nestingLevel, boolOutcome, this) }
            return boolOutcome
        }
    }
    class and(val nestingLevel: Int, val description: String, val andExprs: MutableList<BoolExpr> = mutableListOf()) : BoolExpr() {
        private val log = KotlinLogging.logger(this::class.qualifiedName!!)
        override fun toString(): String = "${this::class.simpleName}[description:${description}, #exprs=${andExprs.size}"
        fun add(vararg expr: BoolExpr): BoolExpr { andExprs.addAll(expr) ; return this }
        fun add(exprs: List<BoolExpr>): BoolExpr { andExprs.addAll(exprs) ; return this }
        override fun eval(ctx: Context): Boolean {
            log.debug { String.format("eL%d->       %s", nestingLevel, this) }
            val boolOutcome = andExprs.firstOrNull { !it.eval(ctx) } == null
            log.debug { String.format("eL%d<- %-5s %s", nestingLevel, boolOutcome, this) }
            return boolOutcome
        }
    }
    class eq<T>(val nestingLevel: Int, val fieldRef: String, val value: T, val description: String = "noDescription"): BoolExpr() {
        private val log = KotlinLogging.logger(this::class.qualifiedName!!)
        override fun toString(): String = "${this::class.simpleName}[fieldRef:${fieldRef}, value:${value}, description:${description}]"
        override fun eval(ctx: Context): Boolean {
            val dtoFieldValueAny: Any
            val boolOutcome = when(value) {
                is String -> {
                    val dtoFieldValue = ctx.dtoStringField(fieldRef)
                    dtoFieldValueAny = dtoFieldValue
                    dtoFieldValue == value
                }
                is Int -> {
                    val dtoFieldValue = ctx.dtoIntField(fieldRef)
                    dtoFieldValueAny = dtoFieldValue
                    dtoFieldValue == value
                }
                else -> throw RuleDslEvalException("unknown datatype on Rule Evaluation Context for $this")
            }
            log.debug { String.format("eL%d:  %-5s %s", nestingLevel, boolOutcome, "dto:\"${dtoFieldValueAny}\" = $this") }
            return boolOutcome
        }
    }
    class eqCtx<T>(val nestingLevel: Int, val fieldRef: String, val ctxFuncName: String, val ctxFuncAndParams: List<Any>, val valueProto: T, val description: String = "noDescription"): BoolExpr() {
        private val log = KotlinLogging.logger(this::class.qualifiedName!!)
        override fun toString(): String = "${this::class.simpleName}[fieldRef:${fieldRef}, ctxFuncName:${ctxFuncName}, ctxFuncParams:${ctxFuncAndParams.drop(1).joinToString("', '", "'", "'")}, value:${valueProto}, description:${description}]"
        override fun eval(ctx: Context): Boolean {
            @Suppress("UNCHECKED_CAST")
            val ctxFunc = ctxFuncAndParams[0] as Context.(String) -> T
            val ctxKey = ctxFuncAndParams[1] as String
            val ctxValue: T = ctx.ctxFunc(ctxKey) // reflection invocation
            val dtoFieldValueAny: Any
            val boolOutcome = when(valueProto) {
                is String -> {
                    val dtoFieldValue = ctx.dtoStringField(fieldRef)
                    dtoFieldValueAny = dtoFieldValue
                    dtoFieldValue == ctxValue
                }
                is Int -> {
                    val fieldValue = ctx.dtoIntField(fieldRef)
                    dtoFieldValueAny = fieldValue
                    fieldValue == ctxValue
                }
                else -> throw RuleDslEvalException("unknown datatype on Rule Evaluation Context for $this")
            }
            log.debug { String.format("eL%d:  %-5s %s", nestingLevel, boolOutcome, "ctx[$ctxKey]:\"$ctxValue\" = dto.$fieldRef:\"$dtoFieldValueAny\" of eqCtx[description:${description}]") }
            return boolOutcome
        }
    }
}
//sealed class LogicalGroupingExpression: IBoolExpr {
//    class BEGIN(): BoolExpr
//    class END(): BoolExpr
//}
interface IType
sealed class TypedValue: IType {
    class VString(val value: String): TypedValue()
    class VInt(val value: Int): TypedValue()
    class VDate(val value: Instant): TypedValue()
}

/** a Statement does not evaluate to a value, instead it is some kind of computing command (e.g. CASE_INSENSITIVE) */
interface IStatement

//class Expr(val expr: BooleanExpression) {
//    fun eval(ctx: Context): Boolean {
//        return expr.eval(ctx)
//    }
//}
