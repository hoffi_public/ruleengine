package com.hoffi.dsl.expr

import com.hoffi.dsl.dto.IDTO
import com.hoffi.dsl.rule.readInstanceProperty

class Context(val contextName: String, val dto: IDTO = IDTO.NULL) {
    companion object { val NULL = Context("NULL") }

    val ctxMap = mutableMapOf<String, Any>()

    fun ctxGetString(key: String): String {
        return ctxMap[key] as String
    }

    fun someFuncWithPar(par: String): String {
        return "ctx.someFuncWithPar(\"$par\")"
    }

    fun dtoStringField(fieldName: String): String = readInstanceProperty(dto, fieldName)
    fun dtoIntField(fieldName: String): Int = readInstanceProperty(dto, fieldName)
}
