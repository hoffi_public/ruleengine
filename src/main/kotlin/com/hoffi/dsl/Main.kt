package com.hoffi.dsl

import com.hoffi.dsl.dto.DataSetDTO
import com.hoffi.dsl.expr.Context
import com.hoffi.dsl.rule.RuleExecutionStrategy
import com.hoffi.dsl.rule.rule
import mu.KotlinLogging

fun main(args: Array<String>) {
    try {
        App().doIt(args)
    } catch(e: Exception) {
        e.printStackTrace()
    }
}

class App {
    private val log = KotlinLogging.logger(this::class.qualifiedName!!)
    fun doIt(args: Array<String>) {

//        val compiledRule = CompileDSL.compileRule("Rule1")
//        println(compiledRule.receipt())
//        println("\n=======================================================\n")


//        a = "b"            AND (d = "else" OR d = "other")
//        a = "b" OR a = "c" AND (d = "else" OR d = "other")



        val rule1 = rule(
            ruleName = "Rule1",
            RuleExecutionStrategy.FirstMatchingTestExecutesActions
            // currently, all preConditionActions are executed, even if the condition afterwards evaluates to false
        ) {
            own UUID "bb70b451-9ce1-477e-a650-ae2984225f82"
            own InputType "DataSetDTO"
            own DummyContext Context("DummyDataSetDTO", DataSetDTO()) // for code completion and checking fields/functions access used in rule(s)
            val dto = this.ctx.dto as DataSetDTO

            Test("direct or Special") {
                preConditionActions {
                    //ctxSet("field2Substring", ctxDtoGetStringField(dto::field2).substring(5, 6))
                    ctxSet("someKey", "someValue")
                    ctxSet("field2Substring", dto::field2)
                    val field2Substring = ctxGetString("field2Substring")      // this doesn't work as Context here is the dummy
                    ctxSet("field2Substring", field2Substring.substring(4, 7)) // so the rule in fact will compare against the static string
                    val stringResult = ctxSomeFuncWithPar(Context::someFuncWithPar, "paramValue") // the ctx.func will be called on rule.evaluate()
                }
                condition {
                    // expressions directly under condition { are implicitly and'ed
                    // comparing dto.field1 value to ctx["someKey"]
                    not { eqCtx(dto::field1, Context::ctxGetString, "someKey", "dummy", description = "XXXXsearchForThisXXX") }
                    eq(dto::field1, "field1Value")
                    eq(dto::fieldInt, 3)
                    eq(dto::dataSetInnerDTO, dto.dataSetInnerDTO::inner, "innerField")
                    not { eq("fieldInt", 42) }
                    and("OROROROROROROROR") {
                        or {
                            eq("field1", "special")
                            and {
                                //eq("field2", "2", "substr(5,6)")
                                eq("field3", "field3Value")
                                eq("field4", "field4Value")
                                //after("field4", (now - 60days)
                            }
                        }
                        not { eq("fieldInt", 43) }
                        //dto.dataSetInnerDTO.inner // <-- intelliJ IDEA autocompletion
                        eq("dataSetInnerDTO.inner", "innerField")
                        eq("fieldInt", 3)
                        eq("fieldInt", 666, "fails")
                    }
                }
                actions {
                    "field1" set "${this@rule.ruleName} has set this value"
                }
                routing {
                    targetExchange("RMQ_Exch_Name")
                    "key1" set "businessProcessName"
                    "key2" set "processID4321"
                    "key3" set "serviceName"
                    "key4" set "serviceSubname"
                }
            }
        }



        val dataSetDTO = DataSetDTO()
        log.debug{""}
        log.debug{"Rule1.evaluate:"}
        val outcome = rule1.evaluate(Context("rule1MainContext", dataSetDTO))
        log.debug{ "Outcome: $outcome of rule1.evaluate(ctx)" }
//        log.trace { "trace Test" }
//        log.info { "info Test" }
//        log.warn { "warn Test" }
//        log.error{" eerror Test"}
    }
}

