package com.hoffi.dsl.rule

import com.hoffi.dsl.dto.IDTO
import com.hoffi.dsl.dto.NullDTO
import com.hoffi.dsl.ex.RuleDslException
import com.hoffi.dsl.expr.BoolExpr
import com.hoffi.dsl.expr.Context
import com.hoffi.dsl.rule.RuleExecutionStrategy.AllMatchingTestsExecuteActions
import mu.KotlinLogging
import java.util.*
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty0
import kotlin.reflect.KProperty1

fun rule(ruleName: String, executionStrategy: RuleExecutionStrategy, theRule: Rule.() -> Unit): Rule =
    Rule(ruleName, AllMatchingTestsExecuteActions).apply(theRule)

val NULLUUID = UUID(0L, 0L)

enum class RuleExecutionStrategy { AllMatchingTestsExecuteActions, FirstMatchingTestExecutesActions }

@DslMarker annotation class DslRule

@DslRule
class Rule(val ruleName: String, val executionStrategy: RuleExecutionStrategy) {
    private val log = KotlinLogging.logger(this::class.qualifiedName!!)
    private var dtoClassNotFound = false
    private val validationErrors = mutableListOf<String>()
    val own = this
    var uuid: UUID = NULLUUID
    var ctx: Context = Context.NULL
    var dtoClass: KClass<out IDTO> = NullDTO::class

    val tests = mutableListOf<Test>()


    infix fun UUID(uuidString: String) {
        this.uuid = UUID.fromString(uuidString)
    }
    fun UUID(): UUID = this.uuid
    infix fun InputType(dtoTypeName: String) {
        try {
            @Suppress("UNCHECKED_CAST")
            dtoClass = Class.forName("com.hoffi.dsl.dto.${dtoTypeName}").kotlin as KClass<out IDTO>
        } catch (e: ClassNotFoundException) {
            dtoClassNotFound = true
            validationErrors.add("InputType: '${dtoTypeName}' does not exist")
        }
    }
    fun InputType(): String = this.InputType()
    infix fun DummyContext(ctx: Context) {
        this.ctx = ctx
    }


    fun Test(name: String = "default", testClosure: Test.() -> Unit): Rule {
        val test = Test(name)
        tests.add(test)
        test.apply(testClosure)
        return this
    }

    fun evaluate(ctx: Context): Boolean {
        validate()
//        val inner = readInstanceProperty<String>(dto, "dataSetInnerDTO.inner")
//        return "field1.inner = ${inner}"
        for(test in tests) {
            log.debug{"preAction Test: ${test.name}"}
            test.preConditionActions.forEach { preAction ->
                preAction.preConditionActions.forEach { action ->
                    log.debug{"preAction: $action"}
                    when (action.op) {
                        ContextFunction.ctxGetString -> {
                            ctx.ctxMap[action.key]
                        }
                        ContextFunction.ctxSet -> {
                            ctx.ctxMap[action.key] = action.value
                        }
                        ContextFunction.ctxSetToField -> {
                            ctx.ctxMap[action.key] = readInstanceProperty(ctx.dto, action.value as String)
                        }
                        ContextFunction.ctxSomeFuncWithPar -> {
                            @Suppress("UNCHECKED_CAST")
                            val defList = action.value as List<Any>
                            @Suppress("UNCHECKED_CAST")
                            val ctxFunc = defList[0] as Context.(String) -> String
                            val funcResult = ctx.ctxFunc(defList[1] as (String)) //ctxFunc.invoke(defList[1] as (String))
                            log.info("preAction: Context.${action.key}(${defList[1]}) -> '${funcResult}'")
                        }
                    }
                }
            }
        }
        for(test in tests) {
            log.debug { "=>eval Test: ${test.name}" }
            var testOutcome = false
            for(cond in test.conditions) {
                testOutcome = true
                for (expr in cond.conditionExprs) {
                    log.debug{"-> TL       $expr"}
                    val exprOutcome = expr.eval(ctx)
                    testOutcome = testOutcome && exprOutcome
                    //log.debug{String.format("<- TL %-5s (Test:%5s) ${expr}", exprOutcome, testOutcome)}
                    log.debug { String.format("<- TL %-5s %s", exprOutcome, expr) }
                    if (!exprOutcome) { log.debug { "TL break" } ; break }
                }
                log.debug { "<=${testOutcome} of eval Test: ${test.name}" }
            }
        }
        return false
    }
    fun validate() {
        if (uuid == NULLUUID) { validationErrors.add("missing UUID") }
        if (!dtoClassNotFound && dtoClass.simpleName == "NullDTO") { validationErrors.add("missing InputType") }

//        val iDTO = dtoClass.createInstance()
//        tests.forEach {
//            it.conditions.forEach {
//                it.conditionExprs.forEach {
//                    it.evals.forEach {
//                        try {
//                            val value: Any = readInstanceProperty(iDTO, it.fieldRef) // check if field exists
//                            if (value::class.simpleName != it.type) {
//                                validationErrors.add("${it.fieldRef} is ${value::class.simpleName} but compared to ${it.type}")
//                            }
//                        } catch(e: NoSuchElementException) {
//                            validationErrors.add(e.message ?: "unknown")
//                        }
//                    }
//                }
//            }
//        }
        if (validationErrors.isNotEmpty()) {
            throw RuleDslException("Rule: '${ruleName}: " + validationErrors.joinToString("', '", "${ruleName}: '", "'"))
        }
    }
}

@DslRule
class Test(val name: String) {
    val preConditionActions = mutableListOf<PreConditionActions>()
    val conditions = mutableListOf<Condition>()
    val actions = mutableListOf<Actions>()
    val routings = mutableListOf<Routing>()

    fun preConditionActions(name: String = "PreConditionActions", preConditionActionsClosure: PreConditionActions.() -> Unit): Test {
        val preConditionActions = PreConditionActions(name)
        this.preConditionActions.add(preConditionActions)
        preConditionActions.apply(preConditionActionsClosure)
        return this
    }

    fun condition(name: String = "TestCondition", conditionClosure: Condition.() -> Unit): Test {
        val condition = Condition(name)
        conditions.add(condition)
        condition.apply(conditionClosure)
        return this
    }

    fun actions(name: String = "Actions", actionsClosure: Actions.() -> Unit): Test {
        val actions = Actions(name)
        this.actions.add(actions)
        actions.apply(actionsClosure)
        return this
    }

    fun routing(name: String = "Routing", routingClosure: Routing.() -> Unit): Test {
        val routing = Routing(name)
        routings.add(routing)
        routing.apply(routingClosure)
        return this
    }
}

@DslRule
class Condition(val name: String, val nestingLevel: Int = 0, val conditionExprs: MutableList<BoolExpr> = mutableListOf()) {
    private val log = KotlinLogging.logger(this::class.qualifiedName!!)

    override fun toString(): String {
        return "${Condition::class.simpleName}[L${nestingLevel}, name:${name}, #exprs:${conditionExprs.size} (${conditionExprs.map { it.toString() }.joinToString("', '", "'", "'")})]"
    }

    fun not(description: String = "noDescription", expressionClosure: Condition.() -> Unit): Condition {
        val notExpr = BoolExpr.not(nestingLevel, description)
        conditionExprs.add(notExpr)
        log.debug{"rL${nestingLevel}-> not[description:${description}]"}

        val innerCondition = Condition(description, nestingLevel+1)
        innerCondition.apply(expressionClosure)

        notExpr.add(innerCondition.conditionExprs)
        log.debug{"rL${nestingLevel}<- $notExpr"}
        return innerCondition

//        val notExpression = BoolExpr.not()
//        conditionExprs.add(notExpression)
//        notExpression.apply(expressionClosure)
//        return notExpression.set()
//        //return this
    }
    fun or(description: String = "noDescription", expressionClosure: Condition.() -> Unit): Condition {
        val orExpr = BoolExpr.or(nestingLevel, description)
        conditionExprs.add(orExpr)
        log.debug{"rL${nestingLevel}-> or[description:${description}]"}

        val innerCondition = Condition(description, nestingLevel+1)
        innerCondition.apply(expressionClosure)

        orExpr.add(innerCondition.conditionExprs)
        log.debug{"rL${nestingLevel}<- $orExpr"}
        return innerCondition
        //return this
    }
    fun and(description: String = "noDescription", expressionClosure: Condition.() -> Unit): Condition {
        val andExpr = BoolExpr.and(nestingLevel, description)
        conditionExprs.add(andExpr)
        log.debug{"rL${nestingLevel}-> and[description:${description}]"}

        val innerCondition = Condition(description, nestingLevel+1)
        innerCondition.apply(expressionClosure)

        andExpr.add(innerCondition.conditionExprs)
        log.debug{"rL${nestingLevel}<- $andExpr"}
        return innerCondition
        //return this
    }
    fun <T> eq(field: KMutableProperty0<T>, value: T, description: String = "noDescription") {
        log.debug{"rL${nestingLevel}:  eq(strF): ${field.name} eq $value ($description)"}
        conditionExprs.add(BoolExpr.eq(nestingLevel, field.name, value, description))
    }
    fun <T> eq(field: KMutableProperty0<out IDTO>, innerField: KMutableProperty0<T>, value: String, description: String = "noDescription") {
        log.debug{"rL${nestingLevel}: eq(strF): ${field.name}.${innerField.name} eq $value ($description)"}
        conditionExprs.add(BoolExpr.eq(nestingLevel, "${field.name}.${innerField.name}", value, description))
    }
    fun eq(fieldRef: String, value: String, description: String = "noDescription") {
        log.debug{"rL${nestingLevel}:  eq(str): $fieldRef eq $value ($description)"}
        //evals.add(AtomicExpression(this, "eq", value, "String"))
        conditionExprs.add(BoolExpr.eq(nestingLevel, fieldRef, value, description))
    }
    fun eq(fieldRef: String, value: Int, description: String = "noDescription") {
        log.debug{"rL${nestingLevel}:  eq(int): $fieldRef eq $value ($description)"}
        //evals.add(AtomicExpression(this, "eq", value, "Int"))
        conditionExprs.add(BoolExpr.eq(nestingLevel, fieldRef, value, description))
    }
    fun <T> eqCtx(field: KMutableProperty0<String>, ctxFunc: Context.(String) -> T, key: String, valueProto: T, description: String = "noDescription") {
        log.debug{"rL${nestingLevel}:  eqCtx(${key}): ${field.name} eqCtx ctx[\"key\"] ($description)"}
        conditionExprs.add(BoolExpr.eqCtx(nestingLevel, field.name, "ctxFunc: Context.(String) -> String", listOf<Any>(ctxFunc, key), valueProto, description))
    }
}

enum class ContextFunction { ctxGetString, ctxSet, ctxSetToField, ctxSomeFuncWithPar }
@DslRule
class PreConditionActions(val name: String) {
    private val log = KotlinLogging.logger(this::class.qualifiedName!!)
    val preConditionActions = mutableListOf<PreConditionActionEval>()

    fun ctxGetString(key: String, description: String = "noDescription"): String {
        preConditionActions.add(PreConditionActionEval(key, ContextFunction.ctxGetString, "unused", description))
        return "Very long dummy so that in case you do something like substring on it, you won't run out of bounds"
    }
    fun ctxSet(key: String, value: String, description: String = "noDescription") {
        preConditionActions.add(PreConditionActionEval(key, ContextFunction.ctxSet, value, description))
    }
    fun <T> ctxSet(key: String, field: KMutableProperty0<T>, description: String = "noDescription") {
        preConditionActions.add(PreConditionActionEval(key, ContextFunction.ctxSetToField, field.name, description))
    }
    fun <T> ctxSet(key: String, field: KMutableProperty0<out IDTO>, innerField: KMutableProperty0<T>, value: String, description: String = "noDescription") {
        preConditionActions.add(PreConditionActionEval(key, ContextFunction.ctxSet, "${field.name}.${innerField.name}", description))
    }
    fun ctxSomeFuncWithPar(ctxFunc: Context.(String) -> String, param: String, description: String = "noDescription"): String {
        preConditionActions.add(PreConditionActionEval(Context::someFuncWithPar.name, ContextFunction.ctxSomeFuncWithPar, listOf<Any>(ctxFunc, param), description))
        return "dummy"
    }
}
data class PreConditionActionEval(val key: String, val op: ContextFunction, val value: Any, val description: String)

@DslRule
class Actions(val name: String) {
    val actions = mutableListOf<ActionEval>()

    infix fun String.set(value: String) {
        actions.add(ActionEval(this, "set", value))
    }
}
data class ActionEval(val fieldRef: String, val op: String, val value: String)

@DslRule
class Routing(val name: String) {
    var targetExchangeName: String = "default"
    val routings = mutableListOf<RoutingEval>()

    fun targetExchange(exchangName: String) {
        targetExchangeName = exchangName
    }

    infix fun String.set(value: String) {
        routings.add(RoutingEval(this, "set", value))
    }
}

data class RoutingEval(val key: String, val op: String, val value: String)

//fun <T> cast(any: Any, clazz: KClass<*>): T = clazz.java.cast(any)
fun <T: IDTO> cast(any: Any, clazz: KClass<out T>): T = clazz.javaObjectType.cast(any)

@Suppress("UNCHECKED_CAST")
fun <R> readInstanceProperty(dto: IDTO, propertyName: String): R {
    val soFar = mutableListOf<String>()
    val refList = propertyName.split('.').toMutableList()
    val lastRef = refList.removeLast()
    var curDTO = dto
    var property: KProperty1<Any, *>
    for (propName: String in refList) {
        soFar.add(propName)
        property = try {
            curDTO::class.members
                // don't cast here to <Any, R>, it would succeed silently
                .first { it.name == propName } as KProperty1<Any, *>
        } catch (e: NoSuchElementException) {
            throw NoSuchElementException("${dto::class.simpleName} does not have a field '${soFar.joinToString(".")}'") // TODO
        }
        // force a invalid cast exception if incorrect type here
        curDTO = property.get(curDTO) as IDTO
    }
    soFar.add(lastRef)
    property = try {
        curDTO::class.members
            // don't cast here to <Any, R>, it would succeed silently
            .first { it.name == lastRef } as KProperty1<Any, *>
    } catch (e: NoSuchElementException) {
        throw NoSuchElementException("${dto::class.simpleName} does not have a field '${soFar.joinToString(".")}'") // TODO
    }
    // force a invalid cast exception if incorrect type here
    return property.get(curDTO) as R
}
