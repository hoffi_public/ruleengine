package com.hoffi.dsl.ex

open class DslException(message:String): Exception(message)

class RuleDslException(message:String): DslException(message)

class RuleDslEvalException(message:String): DslException(message)
