package com.hoffi.dsl

import com.hoffi.dsl.sandwich.Sandwich
import mu.KotlinLogging
import java.io.File
import javax.script.ScriptEngineManager

interface ICompileDSL {
    fun compileSandwich() : Sandwich
    fun compileRule(ruleFilename: String) : Sandwich
}
object CompileDSL : ICompileDSL {
    private val log = KotlinLogging.logger(this::class.qualifiedName!!)
    override fun compileSandwich() : Sandwich {
        val engine = ScriptEngineManager().getEngineByExtension("kts") ?: throw Exception("kts script engine not found")
        log.debug{engine.eval("21 + 21")}
        val dslFileContent = File("./data/Sandwich1.kts").readText()
        val script = """
            import com.hoffi.dsl.sandwich.sandwich
            $dslFileContent
        """.trimIndent()
        val res = engine.eval(script)
        return res as Sandwich
    }

    override fun compileRule(ruleFilename: String) : Sandwich {
        val engine = ScriptEngineManager().getEngineByExtension("kts") ?: throw Exception("kts script engine not found")
        val dslFileContent = File("./data/${ruleFilename}.kts").readText()
        val script = """
            import com.hoffi.dsl.rule.rule
            $dslFileContent
        """.trimIndent()
        val res = engine.eval(script)
        return res as Sandwich
    }
}
